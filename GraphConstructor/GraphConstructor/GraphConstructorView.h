#pragma once

#ifndef GRAPHCONSTRUCTORVIEW_H
#define GRAPHCONSTRUCTORVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>

class Node;
class Edge;

//class for manage graph construction
class GraphConstructorView : public QGraphicsView
{
public:
	GraphConstructorView();

protected:
	void CreateNodeAtPosition( QPointF pos );

	virtual void mouseMoveEvent( QMouseEvent *event ) override;
	virtual void mousePressEvent( QMouseEvent *event ) override;
	virtual void mouseReleaseEvent( QMouseEvent *event ) override;

protected:
	QGraphicsScene* mScene;

	std::list<Node*> mNodes;
	std::list<Edge*> mEdges;

	bool bNewEdgeIsCreating = false;

	/////Timer staff for animation purpose
	QTimer* mTimer;
};

#endif