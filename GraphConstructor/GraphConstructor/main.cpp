#include "GraphConstructorView.h"
#include <QtWidgets/QApplication>

int main( int argc, char *argv[] )
{
	QApplication a( argc, argv );
	GraphConstructorView w;
	w.setSceneRect( 0, 0, 1200, 700 );

	w.show();
	return a.exec();
}
