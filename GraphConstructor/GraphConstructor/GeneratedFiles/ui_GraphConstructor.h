/********************************************************************************
** Form generated from reading UI file 'GraphConstructor.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRAPHCONSTRUCTOR_H
#define UI_GRAPHCONSTRUCTOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GraphConstructorClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *GraphConstructorClass)
    {
        if (GraphConstructorClass->objectName().isEmpty())
            GraphConstructorClass->setObjectName(QString::fromUtf8("GraphConstructorClass"));
        GraphConstructorClass->resize(600, 400);
        menuBar = new QMenuBar(GraphConstructorClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        GraphConstructorClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(GraphConstructorClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        GraphConstructorClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(GraphConstructorClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        GraphConstructorClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(GraphConstructorClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        GraphConstructorClass->setStatusBar(statusBar);

        retranslateUi(GraphConstructorClass);

        QMetaObject::connectSlotsByName(GraphConstructorClass);
    } // setupUi

    void retranslateUi(QMainWindow *GraphConstructorClass)
    {
        GraphConstructorClass->setWindowTitle(QApplication::translate("GraphConstructorClass", "GraphConstructor", nullptr));
    } // retranslateUi

};

namespace Ui {
    class GraphConstructorClass: public Ui_GraphConstructorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRAPHCONSTRUCTOR_H
