#include "Edge.h"
#include "Node.h"
#include <QtGui>

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>

Edge::Edge( QPointF startPosition, QGraphicsItem* parent )
	:
	QGraphicsItem( parent ),
	mSourceNode( nullptr ),
	mDestinationNode( nullptr ),

	mStartPoint( startPosition ),
	mFinishPoint( startPosition ),
	bIsDraft( true )
{
	setFlag( QGraphicsItem::ItemIsFocusable );
	setZValue( -1 );
}

void Edge::SetDestinationNode( Node * destinationNode )
{
	mDestinationNode = destinationNode;
}
void Edge::SetSourceNode( Node * sourceNode )
{
	mSourceNode = sourceNode;
}

QPointF Edge::GetStartPosition() const
{
	if( mSourceNode )
	{
		return mSourceNode->GetCenterPosition();
	}
	else
	{
		return mStartPoint;
	}
}
QPointF Edge::GetFinishPosition() const
{
	if( bIsDraft )
	{
		return mFinishPoint;
	}
	else
	{
		return mDestinationNode->GetCenterPosition();
	}
}

void Edge::paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{
	//infrastructure for shape
	QPen graypen( Qt::gray );
	graypen.setWidth( 2 );
	QPen redpen( Qt::red );
	redpen.setWidth( 3 );

	painter->setPen( graypen );

	//Draw line
	//calculate positions between shapes of nodes
	//center of node + normalized vector of path * radius
	QVector2D pathVector = QVector2D( GetFinishPosition() - GetStartPosition() );

	QPointF source = GetStartPosition() + (pathVector.normalized().toPointF() * 25);

	QPointF destination;
	if( bIsDraft )
	{
		destination = GetFinishPosition();
	}
	else
	{
		destination = GetFinishPosition() - (pathVector.normalized().toPointF() * 25);
	}

	painter->drawLine( /*QPointF(0,0), QPointF(100,100)*/  source, destination );

	//Draw arrow
	//detect angle
	double angle = std::atan2( -pathVector.y(), pathVector.x() );

	//create points and calculate rotation
	QPointF destArrowP1 = destination + QPointF( sin( angle - M_PI / 3 ) * 10, cos( angle - M_PI / 3 ) * 10 );
	QPointF destArrowP2 = destination + QPointF( sin( angle - M_PI + M_PI / 3 ) * 10, cos( angle - M_PI + M_PI / 3 ) * 10 );

	QPolygonF arrow;
	arrow.push_back( destination );
	arrow.push_back( destArrowP1 );
	arrow.push_back( destArrowP2 );

	painter->setBrush( Qt::gray );
	painter->drawPolygon( arrow );
}

QRectF Edge::boundingRect() const
{
	return QRectF( 0, 0, 1200, 700 );
}

void Edge::mousePressEvent( QGraphicsSceneMouseEvent * event )
{
	update();
	QGraphicsItem::mousePressEvent( event );
}
void Edge::mouseReleaseEvent( QGraphicsSceneMouseEvent * event )
{
	update();
	QGraphicsItem::mouseReleaseEvent( event );
}
void Edge::mouseMoveEvent( QGraphicsSceneMouseEvent * event )
{
	update();
	QGraphicsItem::mouseMoveEvent( event );
}

void Edge::advance( int phase )
{
	update();

	if( !bIsDraft )
	{
		if( mSourceNode )
		{
			if( mDestinationNode->GetNodeIsAboutToTerminate() || mSourceNode->GetNodeIsAboutToTerminate() )
			{
				//legal way to delete itself from scene
				this->scene()->removeItem( this );
			}
		}
	}
}
