#include "GraphConstructorView.h"
#include "Node.h"
#include "Edge.h"
#include <QMouseEvent>
#include <QTimer>

GraphConstructorView::GraphConstructorView()
	:
	QGraphicsView()
{
	mScene = new QGraphicsScene();
	setScene( mScene );

	mTimer = new QTimer( mScene );
	QGraphicsScene::connect( mTimer, SIGNAL( timeout() ), mScene, SLOT( advance() ) );
	mTimer->start( 30 );

	QGraphicsTextItem * mText1 = new QGraphicsTextItem( "Left mouse button - create node" );
	mText1->setPos( 10, 10 );
	QGraphicsTextItem * mText2 = new QGraphicsTextItem( "Left mouse button dragging - move node" );
	mText2->setPos( 10, 20 );
	QGraphicsTextItem * mText3 = new QGraphicsTextItem( "Right mouse button - delete node" );
	mText3->setPos( 10, 30 );
	QGraphicsTextItem * mText4 = new QGraphicsTextItem( "Right mouse button dragging - set connection edge" );
	mText4->setPos( 10, 40 );

	mScene->addItem( mText1 );
	mScene->addItem( mText2 );
	mScene->addItem( mText3 );
	mScene->addItem( mText4 );

}

void GraphConstructorView::CreateNodeAtPosition( QPointF pos )
{
	mNodes.push_back( new Node( mNodes.size() + 1 ) );
	mNodes.back()->setPos( pos );
	mScene->addItem( mNodes.back() );
}

void GraphConstructorView::mouseMoveEvent( QMouseEvent * event )
{
	//This code is setting cursor position for edge which is in draft state,
	//i.e. not connected yet
	Edge* test = nullptr;
	QList<QGraphicsItem*> edges = mScene->items();
	foreach( QGraphicsItem* it, edges )
	{
		test = dynamic_cast<Edge*>(it);
		if( test )
		{
			if( test->GetIsDraft() )
			{
				test->SetFinishPosition( event->pos() );
			}
		}
	}

	update();
	QGraphicsView::mouseMoveEvent( event );
}
void GraphConstructorView::mousePressEvent( QMouseEvent * event )
{
	//check if there is any Nodes beyond cursor
	Node* test = nullptr;
	test = dynamic_cast<Node*>(itemAt( event->pos() ));

	//Create new node
	//if there is no and lmb pressed, create one
	if( event->button() == Qt::LeftButton && !test )
	{
		CreateNodeAtPosition( event->pos() - QPointF( 25, 25 ) );
	}

	//If there is node beyond and rmb presses = create new edge
	if( event->button() == Qt::RightButton && test )
	{
		//Create new edge
		bNewEdgeIsCreating = true;

		mEdges.push_back( new Edge( event->pos() ) );

		mEdges.back()->SetSourceNode( test );
		mScene->addItem( mEdges.back() );
		mEdges.back()->setZValue( -1 );

	}

	for( auto& it = mNodes.begin(); it != mNodes.end(); ++it )
	{
		if( *it == nullptr )
		{
			it = mNodes.erase( it );
		}
	}
	for( auto& it = mEdges.begin(); it != mEdges.end(); ++it )
	{
		if( *it == nullptr )
		{
			it = mEdges.erase( it );
		}
	}

	update();
	QGraphicsView::mousePressEvent( event );
}
void GraphConstructorView::mouseReleaseEvent( QMouseEvent * event )
{
	//Here we delete node if it was not state of creating edge 
	//Otherwise we set edge connection between nodes or delete edge if connection was not set and beyond cursor was no any node

	//Delete node
	//check if there is any Nodes beyond cursor
	Node* testNode = nullptr;
	testNode = dynamic_cast<Node*>(itemAt( event->pos() ));

	if( bNewEdgeIsCreating )
	{
		if( testNode )
		{
			if( mEdges.back()->GetSourceNode() == testNode )
			{
				mScene->removeItem( mEdges.back() );

				//also delete node as it was supposed to be deleted
				testNode->SetNodeIsAboutToTerimate( true );
			}
			else
			{
				mEdges.back()->SetDestinationNode( testNode );
				mEdges.back()->SetIsDraft( false );
			}
		}
		else
		{
			//Delete unlinked edge
			mScene->removeItem( mEdges.back() );
			mEdges.pop_back();
		}
	}
	else
	{
		//Delete node
		//if there is and rmb pressed, set to delete it
		if( event->button() == Qt::RightButton && testNode )
		{
			testNode->SetNodeIsAboutToTerimate( true );
		}
	}

	bNewEdgeIsCreating = false;

	QGraphicsView::mouseReleaseEvent( event );
}