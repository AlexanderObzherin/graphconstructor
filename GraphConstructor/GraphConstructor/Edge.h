#pragma once

#ifndef EDGE_H
#define EDGE_H

#include <QGraphicsItem>

class Node;

class Edge : public QGraphicsItem
{
public:
	Edge( QPointF startPosition, QGraphicsItem* parent = NULL );

	void SetDestinationNode( Node* destinationNode );
	Node* GetDestinationNode() const { return mDestinationNode; }

	void SetSourceNode( Node* sourceNode );
	Node* GetSourceNode() const { return  mSourceNode; }

	void SetStartPosition( QPointF startPosition ) { mStartPoint = startPosition; }
	void SetFinishPosition( QPointF finishPosition ) { mFinishPoint = finishPosition; }
	QPointF GetStartPosition() const;
	QPointF GetFinishPosition() const;

	inline void SetIsDraft( bool in ) { bIsDraft = in; }
	inline bool GetIsDraft() const { return bIsDraft; }

protected:
	virtual void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) override;
	virtual QRectF boundingRect() const override;

	virtual void mousePressEvent( QGraphicsSceneMouseEvent * event ) override;
	virtual void mouseReleaseEvent( QGraphicsSceneMouseEvent * event ) override;
	virtual void mouseMoveEvent( QGraphicsSceneMouseEvent *event ) override;

	virtual void advance( int phase ) override;

protected:
	Node* mSourceNode;
	Node* mDestinationNode;

	//If destination node is nullptr then it getting cursor position
	QPointF mStartPoint;
	QPointF mFinishPoint;

	//draft is a first state of edge, then there is no destination node
	//and cursor position instead. 
	bool bIsDraft;
};


#endif //EDGE_H
